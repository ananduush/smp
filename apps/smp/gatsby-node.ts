// @ts-check

import { CreatePagesArgs } from "gatsby";
import languages from "./src/data/languages";
import convertSlug from "./src/utils/convertSlug";
import path from "path";

// import { createResolvers } from "./src/scripts/graphql-source-image";

// Take the pages from src/pages and generate pages for all locales, e.g. /index and /en/index
exports.onCreatePage = ({ page, actions }: any) => {
  const { createPage, deletePage } = actions;

  if (page.path.includes("404")) {
    return;
  }

  deletePage(page);

  languages.langs.forEach((lang) => {
    createPage({
      ...page,
      path:
        lang === languages.defaultLangKey
          ? page?.path
          : `/${lang}${page?.path}`,
      context: {
        ...page.context,
        locale: lang,
      },
    });
  });
};

/**
 * @type {import('gatsby').GatsbyNode['createPages']}
 */
exports.createPages = async function ({ actions, graphql }: CreatePagesArgs) {
  const result: any = await graphql(`
    query {
      cms {
        menus {
          localization {
            code
          }
          page {
            id
            title
            slug
          }
        }
        posts {
          id
          title
          localization {
            code
          }
        }
      }
    }
  `);

  // LOCALES
  // const locales = result?.data?.cms?.localizations;
  const posts = result?.data?.cms?.posts;
  const menus = result?.data?.cms?.menus;

  posts?.forEach((post: any) => {
    const { title, id, localization } = post;

    const isDefault = localization.code === languages.defaultLangKey;

    actions.createPage({
      // path: localizedSlug(
      //   post?.localizations?.code,
      //   `/posts/${convertSlug(tmp)}`
      // ),
      path: `/news/${convertSlug(title)}`,
      component: path.resolve(`./src/builder/news/index.tsx`),
      context: {
        id,
        slug: convertSlug(title),
        locale: localization.code,
      },
    });
  });

  // menus?.forEach((page: any) => {
  //   const {
  //     page: { title, id },
  //     localization,
  //   } = page;
  //   const isDefault = localization.code === languages.defaultLangKey;
  //   const tmp = title;
  //   actions.createPage({
  //     // path: localizedSlug(
  //     //   page?.localizations?.code,
  //     //   `pages/${convertSlug(tmp)}`
  //     // ),
  //     path: `${isDefault ? "" : `/${localization.code}`}/pages/${convertSlug(
  //       tmp
  //     )}`,
  //     component: path.resolve(`./src/templates/page.tsx`),
  //     context: {
  //       slug: convertSlug(tmp),
  //       id: id,
  //       locale: localization.code,
  //     },
  //   });
  // });˛
};

// exports.createResolvers = (params: any) => {
//   console.log(params);
//   // createResolvers(params);
// };

exports.onCreateWebpackConfig = function ({ actions }: any) {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src"),
        "@/static": path.resolve(__dirname, "static"),
      },
    },
  });
};
