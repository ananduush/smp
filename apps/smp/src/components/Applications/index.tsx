import { StaticImage } from "gatsby-plugin-image";
import React from "react";

const index = () => {
  return (
    <div className="xl:container my-[2rem] px-[10px]">
      <div className="flex flex-wrap justify-center gap-[2rem] mb-[2rem] mx-auto lg:px-[5rem]">
        <div>
          <StaticImage
            src="../../images/apps.png"
            alt=""
            width={244}
            className="min-w-max"
          />
        </div>
        <div className="">
          <h2>Water Atomization Process</h2>
          <p className="">
            Gears made by the powder metallurgy method provide a
            cost-effectiveness and durableness.
          </p>
        </div>
      </div>
      <div className="flex flex-wrap justify-center gap-[2rem] mb-[2rem] mx-auto lg:px-[5rem]">
        <div>
          <StaticImage
            src="../../images/apps.png"
            alt=""
            width={244}
            className="min-w-max"
          />
        </div>
        <div className="">
          <h2>Water Atomization Process</h2>
          <p className="">
            Gears made by the powder metallurgy method provide a
            cost-effectiveness and durableness.
          </p>
        </div>
      </div>
      <div className="flex flex-wrap justify-center gap-[2rem] mb-[2rem] mx-auto lg:px-[5rem]">
        <div>
          <StaticImage
            src="../../images/apps.png"
            alt=""
            width={244}
            className="min-w-max"
          />
        </div>
        <div className="">
          <h2>Water Atomization Process</h2>
          <p className="">
            Gears made by the powder metallurgy method provide a
            cost-effectiveness and durableness.
          </p>
        </div>
      </div>
    </div>
  );
};

export default index;
