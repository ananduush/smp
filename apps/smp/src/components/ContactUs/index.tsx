import React from "react";

export default function index() {
  const InputStyling = `border-b-[1px] mb-[1rem] border-gray p-[1rem] outline-none duration-300 ease-in-out focus:border-black bg-lightGray`;

  return (
    <div className="container py-[2rem]">
      <div className="lg:grid lg:grid-cols-2 px-[1rem] md:px-[5rem] lg:px-[10rem] lg:gap-[6rem]">
        <form
          action="/contact-us"
          method="POST"
          className=" mb-[4rem] flex flex-col lg:mb-0"
        >
          <input
            type="text"
            name="name"
            placeholder="Your Name"
            className={InputStyling}
          />
          <input
            type="text"
            name="email"
            placeholder="Email"
            className={InputStyling}
          />
          <input
            type="text"
            name="subject"
            placeholder="Telephone"
            className={InputStyling}
          />
          <textarea
            name="Additional information"
            id=""
            cols={30}
            rows={10}
            placeholder="Quick detail"
            className="border-[1px] mb-[1rem] bg-lightGray border-gray p-[1rem] outline-none duration-300 ease-in-out focus:border-black"
          ></textarea>

          <button
            name="Send message"
            className="max-w-max bg-black py-[0.8rem] px-[2rem] font-[600] duration-300 ease-in-out hover:bg-theme hover:text-black text-white"
          >
            Send message
          </button>
        </form>
        <div className="">
          <div className="mb-[1.4rem]  border-lightGray">
            <h1 className="text-[18px] font-[600]">Mongolia</h1>
            <p className="my-[0.8rem] text-gray">
              Regis Place #1001, Chinggis Avenue 33/2, Khoroo 15, Khan-Uul
              District, Ulaanbaatar
            </p>
          </div>
          <div className="mb-[1.4rem]  border-lightGray">
            <h1 className="text-[18px] font-[600]">Singapore</h1>
            <p className="my-[0.8rem] text-gray">
              50 Raffles Place #15-05/06, Singapore Land Tower, Singapore 048623
            </p>
          </div>
          <div className="mb-[1.4rem]  border-lightGray">
            <h1 className="text-[18px] font-[600]">Telephone</h1>
            <p className="my-[0.8rem] text-gray">
              +976-99111495, +976-75331001
            </p>
            <p className="my-[0.8rem] text-gray">www.fortuneitrade.com </p>
          </div>
          <div className="mb-[1.4rem]  border-lightGray">
            <h1 className="text-[18px] font-[600]">Email</h1>
            <p className="my-[0.8rem] text-gray">www.hr.website.mn </p>
          </div>
        </div>
      </div>
    </div>
  );
}
