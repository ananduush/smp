import React, { useEffect, useState } from "react";

import { IoLocationOutline } from "react-icons/io5";
import {
  FaFacebookF,
  FaGoogle,
  FaLinkedin,
  FaTwitter,
  FaYoutube,
  FaAngleUp,
} from "react-icons/fa";
import { AiOutlinePhone } from "react-icons/ai";
import { FiMail } from "react-icons/fi";
import { StaticImage } from "gatsby-plugin-image";

export default function Footer() {
  const [showTopBtn, setShowTopBtn] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 400) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);

  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="relative">
      <div className="w-screen relative bg-white py-[2rem] px-[2rem] xl:container">
        <div className="absolute -top-[1.2rem] right-[2rem]">
          <button
            onClick={goToTop}
            name="Scroll to Top"
            className="bg-black text-white p-[10px] rounded-full duration-300 ease-in-out"
          >
            <FaAngleUp size={20} />
          </button>
        </div>
        <h1 className="text-theme text-[25px] text-center lg:text-center">
          Contact Us
        </h1>
        <div className="lg:grid lg:grid-cols-3 my-[2rem]">
          <div className="py-[1rem] lg:border-r-[1px] min-w-max lg:pr-[2rem] lg:border-gray">
            <textarea
              name=""
              id=""
              className="w-full border-b-[1px] text-[10px]"
              rows={4}
              placeholder="Message"
            ></textarea>
            <div className="text-center lg:text-left">
              <button
                name="Submit"
                className="bg-black mx-auto text-[7.7px] text-white py-[10px] px-[29px] hover:bg-theme my-[2rem] duration-300 ease-in-out"
              >
                SUBMIT
              </button>
            </div>
            <div className="flex justify-center">
              <span className="bg-[#F1F1F1] p-[0.5rem] mr-[1.5rem] rounded-full hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FaFacebookF />
              </span>
              <span className="bg-[#F1F1F1] p-[0.5rem] mr-[1.5rem] rounded-full hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FaTwitter />
              </span>
              <span className="bg-[#F1F1F1] p-[0.5rem] mr-[1.5rem] rounded-full hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FaLinkedin />
              </span>
              <span className="bg-[#F1F1F1] p-[0.5rem] mr-[1.5rem] rounded-full hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FaGoogle />
              </span>
              <span className="bg-[#F1F1F1] p-[0.5rem] lg:mr-[1.5rem] rounded-full hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FaYoutube />
              </span>
            </div>
          </div>
          <div className="lg:border-r-[1px] border-gray lg:px-[2rem]">
            <div className="flex my-[2rem]">
              <span className="bg-[#F1F1F1] h-full my-auto mr-[1.5rem] p-[5px] hover:bg-theme hover:text-white duration-300 ease-in-out">
                <IoLocationOutline />
              </span>

              <p className="text-[13px] my-auto">
                Building #9 DSAA-1 Street, Khooo 20, Bayngol District,
                Ulaanbaatar 16102, Mongolia
              </p>
            </div>
            <div className="flex my-[2rem]">
              <span className="bg-[#F1F1F1] h-full my-auto mr-[1.5rem] p-[5px] hover:bg-theme hover:text-white duration-300 ease-in-out">
                <AiOutlinePhone />
              </span>

              <p className="text-[13px] text-theme my-auto">+(976) 75079900</p>
            </div>
            <div className="flex my-[2rem]">
              <span className="bg-[#F1F1F1] h-full my-auto mr-[1.5rem] p-[5px] hover:bg-theme hover:text-white duration-300 ease-in-out">
                <FiMail />
              </span>

              <p className="text-[13px]  my-auto">info@smp.mn</p>
            </div>
          </div>
          <div className="lg:pl-[2rem] mx-auto my-auto relative">
            <StaticImage
              src="../../images/map.png"
              alt=""
              className="my-auto"
            />
            <StaticImage
              src="../../images/map_qr.png"
              alt=""
              width={60}
              className="my-auto absolute bottom-0 right-0 z-10"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
