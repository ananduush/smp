import { Link } from "gatsby";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

const MenuItems = () => {
  const NavigationStyling = `relative w-full lg:w-max text-[12px] lg:text-[15px] inline-block px-[16px] py-[12px] font-[400] lg:mx-[1.4rem] text-[12px] lg:text-[16px] lg:p-0 lg:border-0 lg:hover:text-theme duration-300 ease-in-out`;
  const ActiveNavigationStyling = `lg:text-theme font-[900] lg:font-[400]`;

  return (
    <div>
      <div
        className={` my-auto relative flex border-[1px] mx-[2rem] bg-theme lg:right-[1rem] lg:top-[4rem] lg:translate-x-0 lg:rounded-lg lg:border-0 lg:border-gray lg:shadow-[0px_0px_25px_0px_#cfcfcf4a] lg:hidden`}
      >
        <input
          className="outline-0 py-[5px] px-[1rem] bg-theme"
          placeholder="Search..."
          type="text"
        ></input>

        <button
          name="Search"
          className="absolute top-[50%] text-[12px] -translate-y-[50%] right-[10px]"
        >
          <AiOutlineSearch
            size={20}
            className="hover:fill-black"
            color="white"
          />
        </button>
        {/* </div> */}
      </div>
      <div className="grid mx-[2rem] lg:mx-0 grid-rows-6 p-[1.5rem] lg:flex lg:p-0 lg:pl-[4rem]">
        <div className="my-auto">
          <Link
            to={`/`}
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            Home
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/about-us/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            About Us
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/technology/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            Technology
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/products/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
            partiallyActive={true}
          >
            Products
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/application/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            Application
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/career/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            Career
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/news/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
            partiallyActive={true}
          >
            News
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/contact-us/"
            className={NavigationStyling}
            activeClassName={ActiveNavigationStyling}
          >
            Contact Us
          </Link>
        </div>
        <div className="my-auto">
          <Link
            to="/"
            className={NavigationStyling}
            // activeClassName={ActiveNavigationStyling}
          >
            English
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MenuItems;
