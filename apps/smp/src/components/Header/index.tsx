import React, { useState } from "react";
import { StaticImage } from "gatsby-plugin-image";
import { Link } from "gatsby";

import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import MenuItems from "./MenuItems";

const Header = () => {
  let [open, setOpen] = useState(false);
  const changeIcon = (state: any) => {
    if (state == true) {
      return false;
    } else {
      return true;
    }
  };

  return (
    <div className={`mx-auto h-full w-screen`}>
      <div className="absolute w-screen lg:bg-white lg:relative lg:flex lg:justify-between container">
        {/* Homepage */}
        <div className="fixed z-30 lg:relative">
          <div className="absolute lg:relative">
            <div className="flex w-screen justify-between bg-white px-[5%] shadow-[0_10px_30px_0px_#cfcfcf4a] lg:block lg:w-fit xl:px-0 lg:shadow-[0_0px_0px_0px_#cfcfcf4a]">
              <Link to="/">
                <StaticImage
                  src="../../images/smp.png"
                  alt=""
                  className="my-[1rem] lg:my-[1.5rem] max-w-[212px]"
                  placeholder="none"
                />
              </Link>

              <button
                name="Menu"
                className="transition-all block my-auto duration-300 ease-in-out lg:hidden"
                onClick={() => {
                  setOpen((old) => changeIcon(old));
                }}
              >
                {open === false ? (
                  <div className="flex">
                    <span className="h-[2rem] border-l-[1px] border-lightGray stroke-[1px]"></span>
                    <AiOutlineMenu
                      size={25}
                      className="my-auto ml-[2rem]"
                      color="lightGray"
                    />
                  </div>
                ) : (
                  <AiOutlineClose size={25} color="gray" />
                )}
              </button>
            </div>
          </div>
        </div>
        <div
          className={`fixed z-20 sm:z-40 hidden sm:pt-[70px] my-auto h-full lg:w-screen bg-theme lg:bg-white text-white lg:text-black  pt-[150px] duration-300 ease-in-out lg:relative lg:grid lg:h-fit lg:grid-cols-2 lg:pt-0 w-screen sm:w-[300px] ${
            open ? "left-0" : "-left-[110%] lg:left-0"
          }`}
        >
          <button
            name="Menu"
            className="transition-all absolute top-[2rem] right-[1.5rem] my-auto border-lightGray duration-300 ease-in-out lg:hidden"
            onClick={() => {
              setOpen((old) => changeIcon(old));
            }}
          >
            {open === false ? (
              <AiOutlineMenu size={25} className="stroke-[1px]" color="gray" />
            ) : (
              <AiOutlineClose size={25} color="white" />
            )}
          </button>
          {/* Navigation */}
          <MenuItems />
        </div>

        {/* Mobile Header */}
        <div className="lg:hidden block">
          <div
            className={`fixed z-20 sm:z-40 sm:pt-[70px] my-auto h-full lg:w-screen bg-theme lg:bg-white text-white lg:text-black  pt-[150px] duration-300 ease-in-out lg:relative lg:grid lg:h-fit lg:grid-cols-2 lg:pt-0 w-screen sm:w-[300px] ${
              open ? "left-0" : "-left-[110%] lg:left-0"
            }`}
          >
            <button
              name="Menu"
              className="transition-all absolute top-[2rem] right-[1.5rem] my-auto border-lightGray duration-300 ease-in-out lg:hidden"
              onClick={() => {
                setOpen((old) => changeIcon(old));
              }}
            >
              {open === false ? (
                <AiOutlineMenu
                  size={25}
                  className="stroke-[1px]"
                  color="gray"
                />
              ) : (
                <AiOutlineClose size={25} color="white" />
              )}
            </button>
            {/* Navigation */}
            <MenuItems />
          </div>
        </div>
        <span
          className={`h-screen w-screen bg-black opacity-40 z-0 sm:z-30 absolute lg:hidden cursor-pointer top-0 ${
            open ? "left-0" : "-left-[110%] lg:left-0"
          }`}
          onClick={() => {
            setOpen((old) => changeIcon(old));
          }}
        ></span>
      </div>
    </div>
  );
};

export default Header;
