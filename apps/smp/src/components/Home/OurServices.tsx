import React from "react";
import { GiFactory, GiMineralPearls, GiMetalBar } from "react-icons/gi";
import { BsMinecartLoaded } from "react-icons/bs";

const OurServices = () => {
  return (
    <div className="">
      <div className="grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-[1rem] lg:grid-cols-4 xl:container">
        {/* Box */}
        <div className="mb-[1rem] flex justify-between bg-white hover:-translate-y-[1rem] hover:bg-theme duration-300 ease-in-out p-[2rem]">
          <div className="mt-[3rem]">
            <h3>Mellurgical coal</h3>
            <ul className="list-disc list-inside mb-[2rem]">
              <li className="">Copper cathode</li>
              <li className="">Copper cathode</li>
              <li className="">Copper cathode</li>
            </ul>
          </div>
          <div className="bg-lightGray h-max p-[0.5rem]">
            <GiFactory className="" size={40} />
          </div>
        </div>

        {/* Box */}
        <div className="mb-[1rem] flex justify-between bg-white hover:-translate-y-[1rem] hover:bg-theme duration-300 ease-in-out p-[2rem]">
          <div className="mt-[3rem]">
            <h3>Minerals</h3>
            <ul className="list-disc list-inside mb-[2rem]">
              <li className="">Fluerspar</li>
              <li className="">Acid spar</li>
            </ul>
          </div>
          <div className="bg-lightGray h-max p-[0.5rem]">
            <GiMineralPearls className="" size={40} />
          </div>
        </div>

        {/* Box */}
        <div className="mb-[1rem] flex justify-between bg-white hover:-translate-y-[1rem] hover:bg-theme duration-300 ease-in-out p-[2rem]">
          <div className="mt-[3rem]">
            <h3>Logistics</h3>
            <ul className="list-disc list-inside mb-[2rem]">
              <li className="">Railway</li>
              <li className="">Trucking</li>
            </ul>
          </div>
          <div className="bg-lightGray h-max p-[0.5rem]">
            <BsMinecartLoaded className="" size={40} />
          </div>
        </div>

        {/* Box */}
        <div className="mb-[1rem] flex justify-between bg-white hover:-translate-y-[1rem] hover:bg-theme duration-300 ease-in-out p-[2rem]">
          <div className="mt-[3rem]">
            <h3>Metals</h3>
            <ul className="list-disc list-inside mb-[2rem]">
              <li className="">Copper cathode</li>
              <li className="">Copper powder</li>
              <li className="">Gold concentrate</li>
              <li className="">Metal ores</li>
            </ul>
          </div>
          <div className="bg-lightGray h-max p-[0.5rem]">
            <GiMetalBar className="" size={40} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurServices;
