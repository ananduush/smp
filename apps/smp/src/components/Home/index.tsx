import React from "react";

import { StaticImage } from "gatsby-plugin-image";

const Home = () => {
  return (
    <div>
      <div className="lg:flex xl:container ">
        <StaticImage
          src="../../images/scientist.png"
          alt=""
          className="drop-shadow-[-35px_35px_0px_rgba(170,53,17,1)] lg:min-w-max mx-auto"
          placeholder="blurred"
          width={700}
        />

        <div className="lg:p-[3rem] lg:mt-0 mt-[2rem]">
          <p className="text-gray text-justify border-b-[1px] border-black pb-[3rem]">
            Our water atomized coppers are conductive to diffeebt segments of
            powder metullurgy and metal injection molding. Customer can use our
            powders for various applications such as brake pads, lubrication
            bearings, welding and brazing electrodes, and carbon brushes.
          </p>
          <h1 className="lg:text-right text-left uppercase text-theme lg:leading-10 my-[1rem]">
            Product <br /> Application
          </h1>
          <p className=" text-justify border-b-[1px] border-black pb-[3rem]">
            Our water atomized coppers are conductive to diffeebt segments of
            powder metullurgy and metal injection molding. Customer can use our
            powders for various applications such as brake pads, lubrication
            bearings, welding and brazing electrodes, and carbon brushes.
          </p>
        </div>
      </div>
      <div className="bg-theme md:px-[2rem] sm:px-[1rem] px-[10px] lg:px-[4rem] py-[2rem] md:py-[4rem] lg:py-[10rem] mt-[3rem]">
        <div className="md:flex gap-[2rem] xl:container">
          <div className="bg-white md:p-[1rem] sm:p-[10px] p-[5px] lg:p-[2rem] mb-[1rem] lg:mb-0">
            <div className="relative">
              <h1 className="text-white absolute z-10 left-[50%]  -translate-x-[50%] top-[50%] -translate-y-[50%]">
                Cu
              </h1>
              <StaticImage
                src="../../images/powder/cu.png"
                alt=""
                placeholder="blurred"
                className=""
              />
            </div>
            <div className="text-center">
              <h2 className="">Copper powder</h2>
              <p>
                Copper content is 98-99% (+1% tin) with size between 45-150
                microns.
              </p>
            </div>
          </div>
          <div className="bg-white md:p-[1rem] sm:p-[10px] p-[5px] lg:p-[2rem] mb-[1rem] lg:mb-0">
            <div className="relative">
              <h1 className="text-white absolute z-10 left-[50%]  -translate-x-[50%] top-[50%] -translate-y-[50%]">
                CuSn
              </h1>
              <StaticImage
                src="../../images/powder/cusn.png"
                alt=""
                placeholder="blurred"
              />
            </div>
            <div className="text-center">
              <h2 className="">Copper powder</h2>
              <p>
                Copper content is 98-99% (+1% tin) with size between 45-150
                microns.
              </p>
            </div>
          </div>
          <div className="bg-white md:p-[1rem] sm:p-[10px] p-[5px] lg:p-[2rem] mb-[1rem] lg:mb-0">
            <div className="relative">
              <h1 className="text-white absolute z-10 left-[50%]  -translate-x-[50%] top-[50%] -translate-y-[50%]">
                CuZn
              </h1>
              <StaticImage
                src="../../images/powder/cuzn.png"
                alt=""
                placeholder="blurred"
              />
            </div>
            <div className="text-center">
              <h2 className="">Copper powder</h2>
              <p>
                Copper content is 98-99% (+1% tin) with size between 45-150
                microns.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
