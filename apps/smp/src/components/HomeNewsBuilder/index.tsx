import React from "react";
import { Link } from "gatsby";

import usePosts from "@/hooks/usePosts";
import convertSlug from "@/utils/convertSlug";

import { IoIosArrowForward } from "react-icons/io";
import { KeystoneImage } from "gatsby-image-keystone";

const Builder = () => {
  const article = usePosts();
  const articleBuilder = article.map((post: any) => {
    return (
      <div
        key={post.id}
        className="bg-white p-[1rem] border-r-[1px] border-theme min-w-[20rem] max-w-[20rem] my-[1rem]"
      >
        <div className="flex gap-3 min-h-[72px]">
          {/* Title */}
          <div className="">
            <Link
              to={`/news/${convertSlug(post.title)}`}
              className="font-[700] duration-300 ease-in-out hover:text-theme"
            >
              {/* <p className="font-[700] leading-[30px]">{article.title}</p> */}
              {/* Title */}
              <p className="font-[700] text-[12px]">{post.title}</p>
            </Link>
          </div>

          {/* Image */}
          <div className="relative h-max bg-theme p-[2px]">
            <a href="relative group">
              {post.featureImage && (
                <KeystoneImage
                  baseUrl="fortune"
                  image={{
                    alt: post.featureImage.id,
                    width: 72,
                    height: 72,
                    key: `${post.featureImage.id}.${post.featureImage.extension}`,
                  }}
                  layout="fixed"
                  alt={post.featureImage.id}
                />
              )}
            </a>
          </div>
        </div>
        <Link to={`/news/${convertSlug(post.title)}`}>
          <div className="relative group flex duration-300 ease-in-out">
            <p className="text-[12px] text-gray font-[700] group-hover:text-theme">
              Read more
            </p>
            <div className="my-auto">
              <IoIosArrowForward size={10} className="group-hover:text-theme" />
            </div>
          </div>
        </Link>
      </div>
    );
  });

  return (
    <div className="overflow-x-scroll flex bg-white mx-auto">
      {articleBuilder}
    </div>
  );
};

export default Builder;
