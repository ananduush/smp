import * as React from "react";

import Header from "../Header";
import Footer from "../Footer";
import { Helmet } from "react-helmet";
import HomeNews from "@/components/HomeNewsBuilder";
import "../../styles/global.css";
import { StaticImage } from "gatsby-plugin-image";

type Props = {
  children: React.ReactNode;
  title: string;
};

const Layout = ({
  children,
  title = `Title here......`,
  activeTab,
  ...rest
}: Props | any) => {
  return (
    <div className="overflow-hidden">
      <Helmet title={title + ` - Steppe Metal Powder`}>
        <link rel="icon" href="" />
      </Helmet>
      <div className="fixed z-20 bg-white">
        <Header />
      </div>
      {/*  */}
      {title === `Home` ? (
        <div>
          <div className="flex pt-[100px]">
            <HomeNews />
          </div>
          <div className="relative mx-auto h-auto bg-lightGray">
            <div className="z-0 mx-auto h-full min-h-screen translate-y-0 px-[10px] overflow-hidden xl:px-0 ">
              {children}
            </div>
          </div>
        </div>
      ) : (
        <div className="relative z-0 mx-auto bg-lightGray min-h-screen lg:px-0 h-full pt-[100px] lg:translate-y-0 lg:pt-[100px]">
          <div className="relative min-h-max">
            <h1 className="text-white absolute top-[50%] left-[50%] -translate-y-[50%] -translate-x-[50%] z-10">
              <span className="text-center lg:text-[30px]">
                {title}
                <div className="">
                  {activeTab === "tab1" ? (
                    <p>Technology &gt; Production Process</p>
                  ) : activeTab === "tab2" ? (
                    <p>Technology &gt; Quality Control and Testing</p>
                  ) : (
                    <div></div>
                  )}
                </div>
              </span>
            </h1>
            <StaticImage
              src="../../images/aboutus/bg1.png"
              alt=""
              className="min-h-[10rem] max-h-[10rem]"
            />
          </div>
          {children}
        </div>
      )}

      {/* <Footer /> */}
      <Footer />
    </div>
  );
};

export default Layout;
