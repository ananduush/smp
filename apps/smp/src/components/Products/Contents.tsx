import { StaticImage } from "gatsby-plugin-image";
import React from "react";

const Contents = (content: any) => {
  return content === "tab1" ? (
    // Content #1
    <div>
      <StaticImage
        placeholder="blurred"
        src="../../images/products/copper.png"
        alt="Water Automization"
        className="min-h-max w-full"
      />
    </div>
  ) : content === "tab2" ? (
    <div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/bronze/bronze1.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/bronze/bronze2.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/bronze/bronze3.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
    </div>
  ) : content === "tab3" ? (
    <div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/brass/brass1.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/brass/brass2.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
      <div>
        <StaticImage
          placeholder="blurred"
          src="../../images/products/brass/brass3.png"
          alt="Water Automization"
          className="min-h-max w-full"
        />
      </div>
    </div>
  ) : (
    <div>error</div>
  );
};

export default Contents;
