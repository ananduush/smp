import React, { useState } from "react";
import Contents from "./Contents";

export default function Technology() {
  const NavigationStyling = `text-[12px] lg:text-[16px] hover:text-white hover:bg-theme px-[10px]`;
  const ActiveNavigationStyling = `text-[12px] lg:text-[16px] text-white bg-theme px-[10px]`;
  const [activeTab, setActiveTab] = useState("tab1");
  const handleTab1 = () => {
    // update the state to tab1
    setActiveTab("tab1");
  };
  const handleTab2 = () => {
    // update the state to tab2
    setActiveTab("tab2");
  };
  const handleTab3 = () => {
    // update the state to tab2
    setActiveTab("tab3");
  };

  return (
    <div className="px-[10px]">
      {/* BG Image */}

      <div className="lg:flex xl:container gap-[2rem] my-[2rem]">
        {/* Tabs */}
        <div className="flex-row min-w-max">
          <h1 className="text-lightTheme mb-[2rem]">Products</h1>
          <div className="max-w-max mx-auto flex-wrap lg:block lg:p-0 cursor-pointer">
            <div className="my-auto">
              <div
                className={
                  activeTab === "tab1"
                    ? ActiveNavigationStyling
                    : NavigationStyling
                }
                onClick={handleTab1}
              >
                <p className="lg:px-[16px] lg:py-[12px] text-[16px] font-[400] lg:p-0">
                  Copper powder
                </p>
              </div>
            </div>
            <div className="my-auto">
              <div
                className={
                  activeTab === "tab2"
                    ? ActiveNavigationStyling
                    : NavigationStyling
                }
                onClick={handleTab2}
              >
                <p className="lg:px-[16px] lg:py-[12px] text-[16px] font-[400] lg:p-0">
                  Bronze powder
                </p>
              </div>
            </div>
            <div className="my-auto">
              <div
                className={
                  activeTab === "tab3"
                    ? ActiveNavigationStyling
                    : NavigationStyling
                }
                onClick={handleTab3}
              >
                <p className="lg:px-[16px] lg:py-[12px] text-[16px] font-[400] lg:p-0">
                  Brass powder
                </p>
              </div>
            </div>
          </div>
        </div>

        {/* Content */}
        <div className=" my-[2rem]">{Contents(activeTab)}</div>
      </div>
    </div>
  );
}
