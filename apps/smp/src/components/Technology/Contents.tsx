import { StaticImage } from "gatsby-plugin-image";
import React from "react";

const Contents = (content: any) => {
  return content === "tab1" ? (
    // Content #1
    <div className="lg:pr-[2rem]">
      {/* Title */}
      <h1 className="uppercase mb-[2rem] text-lightTheme">
        Water Atomization Process
      </h1>
      {/* Paragraph */}
      <p className="text-justify">
        Atomization is the most commercially used process to produce the largest
        tonnage of metal powders. High-pressure water atomization has proven to
        be a viable, low-cost process to achieve fine particle size
        distributions for iron, stainless and low-alloy metal powders. The
        economic advantages and pre-alloying capability provide desirable
        advantages over competing technologies. Previous shortcomings relative
        to powder characteristics, i.e. irregular particle shape, lower tap
        densities, oxidized surfaces, have been refined to more closely
        replicate gas atomized powder properties. <br /> In principle the
        technique is applicable to all metals that can be melted, and is
        commercially used for the production of iron, copper, including tool
        steels, alloy steels, brass, bronze and the low-melting-point metals,
        such as aluminum, tin, lead, zinc, cadmium. If a falling stream of
        molten metal is impinged by jets of water, then it is broken up into
        droplets which rapidly freeze to form granules (&gt; 1 mm) or powder
        (&gt; 1 mm), depending on the composition of the metal alloy, and the
        water pressure. Classical granulation uses pressures in the range of 2–5
        bars (200–500 kPa) and typically produces 1–10 mm size granules. Very
        large flow rates can be accommodated (in the t/min range), as pumping
        costs are modest. <br /> To make finer powders, one needs higher
        pressures (we find median size is approximately inversely correlated
        with pressure), so it is necessary to use more controlled streams,
        typically from a tundish arrangement instead of a launder as in many
        granulators, and to run more modest flow rates, typically using nozzles
        from 3 mm to 30 mm in diameter to give flow rates from 5 kg/min to ~500
        kg/min (up to 30 t/h). Pressures range from 20 bars for coarser powders
        (say 0.3 mm) to 200 bars for finer powders (say ~50 μm). Elements like
        sulphur in the melt strongly affect (reduce) the required pressure by
        reducing melt surface tension. As shown in Figure 2, the atomized slurry
        can be pumped, either directly to leach tanks, or to a dewatering system
        which can deliver either a thickened slurry at ~20% moisture or a damp
        solid at ~5% moisture. This can then be fed to the leach tanks if the
        water balance is critical. Drying adds significant energy and cost, but
        may be done if, for instance, a smelter is selling to a refiner.
      </p>
      <StaticImage
        src="../../images/technology/water.png"
        alt="Water Automization"
        className="my-[2rem] w-full"
      />
      <p className="text-justify">
        Atomization is the most commercially used process to produce the largest
        tonnage of metal powders. High-pressure water atomization has proven to
        be a viable, low-cost process to achieve fine particle size
        distributions for iron, stainless and low-alloy metal powders. The
        economic advantages and pre-alloying capability provide desirable
        advantages over competing technologies. Previous shortcomings relative
        to powder characteristics, i.e. irregular particle shape, lower tap
        densities, oxidized surfaces, have been refined to more closely
        replicate gas atomized powder properties. <br /> In principle the
        technique is applicable to all metals that can be melted, and is
        commercially used for the production of iron, copper, including tool
        steels, alloy steels, brass, bronze and the low-melting-point metals,
        such as aluminum, tin, lead, zinc, cadmium. If a falling stream of
        molten metal is impinged by jets of water, then it is broken up into
        droplets which rapidly freeze to form granules (&gt; 1 mm) or powder
        (&gt; 1 mm), depending on the composition of the metal alloy, and the
        water pressure. Classical granulation uses pressures in the range of 2–5
        bars (200–500 kPa) and typically produces 1–10 mm size granules. Very
        large flow rates can be accommodated (in the t/min range), as pumping
        costs are modest. <br /> To make finer powders, one needs higher
        pressures (we find median size is approximately inversely correlated
        with pressure), so it is necessary to use more controlled streams,
        typically from a tundish arrangement instead of a launder as in many
        granulators, and to run more modest flow rates, typically using nozzles
        from 3 mm to 30 mm in diameter to give flow rates from 5 kg/min to ~500
        kg/min (up to 30 t/h). Pressures range from 20 bars for coarser powders
        (say 0.3 mm) to 200 bars for finer powders (say ~50 μm). Elements like
        sulphur in the melt strongly affect (reduce) the required pressure by
        reducing melt surface tension. As shown in Figure 2, the atomized slurry
        can be pumped, either directly to leach tanks, or to a dewatering system
        which can deliver either a thickened slurry at ~20% moisture or a damp
        solid at ~5% moisture. This can then be fed to the leach tanks if the
        water balance is critical. Drying adds significant energy and cost, but
        may be done if, for instance, a smelter is selling to a refiner.
      </p>
    </div>
  ) : content === "tab2" ? (
    <div>
      {/* Title */}
      <h1 className="uppercase text-lightTheme mb-[2rem]">
        Quality Control And Testing
      </h1>

      <div className="flex flex-wrap mx-auto justify-center lg:justify-start gap-[1rem]">
        {/* Box */}
        <div className="">
          <StaticImage
            src="../../images/bull-market.jpeg"
            className="mb-[1rem]"
            alt=""
            width={300}
            height={192}
          />
          <p className="font-[600] text-[16px]">Microtrac machine</p>
        </div>
        {/* Box */}
        <div className="">
          <StaticImage
            src="../../images/bull-market.jpeg"
            className="mb-[1rem]"
            alt=""
            width={300}
            height={192}
          />
          <p className="font-[600] text-[16px]">ICP-OES Machine</p>
        </div>
        {/* Box */}
        <div className="">
          <StaticImage
            src="../../images/bull-market.jpeg"
            className="mb-[1rem]"
            alt=""
            width={300}
            height={192}
          />
          <p className="font-[600] text-[16px]">O&N Analysis Machine</p>
        </div>
        {/* Box */}
        <div className="">
          <StaticImage
            src="../../images/bull-market.jpeg"
            className="mb-[1rem]"
            alt=""
            width={300}
            height={192}
          />
          <p className="font-[600] text-[16px]">Hall flow meter</p>
        </div>
        {/* Box */}
        <div className="">
          <StaticImage
            src="../../images/bull-market.jpeg"
            className="mb-[1rem]"
            alt=""
            width={300}
            height={192}
          />
          <p className="font-[600] text-[16px]">Hydraulic press</p>
        </div>
      </div>
    </div>
  ) : (
    <div>error</div>
  );
};

export default Contents;
