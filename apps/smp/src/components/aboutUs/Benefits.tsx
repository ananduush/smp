import React from "react";
import { AiOutlineCheckSquare } from "react-icons/ai";

const Benefits = () => {
  return (
    <div>
      <h2 className="text-[25px] border-b-[1px] border-theme pb-[2rem] my-[3rem]">
        SOCIO-ECONOMIC BENEFITS OF SMP PLANT
      </h2>
      <div className="grid sm:grid-cols-2 grid-cols-1 gap-[1rem] lg:grid-cols-3">
        {/* Box */}
        <div className="mb-[1rem] flex gap-[1rem] justify-between bg-white  px-[1rem] pb-[2rem]">
          <div className="mt-[3rem]">
            <h3 className="lg:text-justify lg:text-[18px]">
              SOCIO-ECONOMIC BENEFITS OF SMP PLANT
            </h3>
            <p className="text-justify lg:text-[16px]">
              The plant will establish a base of a &quot;SMART&quot; production
              for automotive parts, industrial equipment and electronics. SMP
              will have an innovation and R&D function to produce new products
              from different types of metal powder.
            </p>
          </div>
          <div className="bg-[#EAEAEA] h-max p-[1rem] mt-[1rem]  text-theme">
            <AiOutlineCheckSquare className="" size={20} />
          </div>
        </div>
        {/* Box */}
        <div className="mb-[1rem] flex gap-[1rem] justify-between bg-theme text-white px-[1rem] pb-[2rem]">
          <div className="mt-[3rem]">
            <h3 className="lg:text-justify lg:text-[18px]">
              EMPLOYMENT & PROCUREMENT
            </h3>
            <p className="text-justify lg:text-[16px]">
              At full capacity, the plant will employ about 20
              highly-specialized personnel creating a unique talent pool in
              Mongolian industry. The construction of plant and ancillary
              facilities including laboratory involved around 50 domestic and 15
              foreign subcontractors.
            </p>
          </div>
          <div className="bg-[#EAEAEA] h-max p-[1rem] text-theme mt-[1rem]">
            <AiOutlineCheckSquare className="" size={20} />
          </div>
        </div>
        {/* Box */}
        <div className="mb-[1rem] flex gap-[1rem] justify-between bg-white  px-[1rem] pb-[2rem]">
          <div className="mt-[3rem]">
            <h3 className="lg:text-justify lg:text-[18px]">
              ENVIRONMENTALLY FRIENDLY OPERATION
            </h3>
            <p className="text-justify lg:text-[16px]">
              SMP’s plant will re-use copper and other metal scraps as feed,
              ultimately adding reasonable value to low-value materials. Also,
              the recirculation rate of process water is more than 90%.
            </p>
          </div>
          <div className="bg-[#EAEAEA] h-max p-[1rem] mt-[1rem]  text-theme">
            <AiOutlineCheckSquare className="" size={20} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Benefits;
