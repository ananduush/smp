import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import Benefits from "./Benefits";

const OurBusiness = () => {
  return (
    <div className="">
      <div className="relative min-h-[22rem]">
        <div className="absolute top-0">
          <StaticImage
            src="../../images/aboutus/bg2.png"
            alt=""
            placeholder="blurred"
            className="min-h-[22rem] max-h-[23rem]"
          />
        </div>
        <div className="relative xl:container text-white">
          <div className="flex gap-[3rem] px-[1rem] justify-center align-middle items-center mx-auto">
            <div className="flex gap-[2.5rem] my-[2rem]">
              <div className="my-auto">
                <StaticImage
                  src="../../images/icon-white.png"
                  alt=""
                  className=""
                  placeholder="blurred"
                  width={420}
                />
              </div>
              <span className="w-[2px] bg-white"></span>
              <h1 className="">
                <span className="font-[400] sm:text-[20px] lg:text-[30px] text-[#9F9FA0]">
                  METAL <br />
                </span>
                <span className="lg:text-[60px] sm:text-[40px]">
                  PODWER <br />
                </span>
                <span className="lg:text-[30px] sm:text-[20px]">PLANT</span>
              </h1>
            </div>
          </div>
          <p className="text-center relative w-[80%] mx-auto md:text-[15px]">
            Steppe Metal Powder LLC was established in 2017. The company owns an
            European standart metal powder production plant in Ulaanbaatar,
            Mongolia. The company is set to produce and sell high-quality copper
            and copper alloy powders for international markets using LME Grade A
            copper attested by an internationally accredited laboratory.
          </p>
        </div>
      </div>
      <div className="lg:flex xl:container lg:mt-[3rem] my-[1rem] px-[10px] lg:px-0">
        <div>
          <StaticImage
            src="../../images/aboutus/plant.png"
            alt=""
            placeholder="blurred"
            className="lg:min-w-max lg:max-h-max lg:drop-shadow-[-35px_35px_0px_#CE6F52]"
            width={700}
          />
        </div>
        <div className="lg:px-[4rem] ">
          <h1 className="uppercase my-[1rem]">Steppe metal powder</h1>
          <p className="text-justify">
            Steppe Metal Powder is an export-oriented production plant with a
            wide network of suppliers, customers, and partners in Europe and
            Asia. Introducing the most advanced British and German technologies
            and equipment, the plant has a production capacity of 3,000 metric
            tons of metal powders per year. It is our long-term objective to
            diversify the product portfolio per market trends and customer
            needs. The metal powders produced by SMP are compatible with the
            needs of the powder metallurgy industry and rapidly developing
            processes like metal injection molding, hot isostatic pressing,
            sintering, and additive manufacturing (3D printing). SMP’s plant and
            laboratory facility are fully equipped with modern technology with
            minimal impact on the environment and the surrounding community. The
            Detailed Environmental Impact Assessment of the SMP plant was
            approved by the authorities in 2020 and the company has pro-actively
            implementing all the actions contemplated in DEIA.
          </p>
        </div>
      </div>
      <div className="xl:container px-[10px]">
        <Benefits />
      </div>
    </div>
  );
};

export default OurBusiness;
