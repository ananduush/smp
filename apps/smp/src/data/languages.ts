const languages = {
  langs: ["en", "mn"],
  defaultLangKey: "en",
};

export default languages;
