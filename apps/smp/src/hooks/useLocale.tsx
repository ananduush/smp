import GlobalContext from "../contexts/GlobalContext";
import React from "react";

const useLocale = () => {
  const { locale } = React.useContext(GlobalContext);
  return locale;
};

export default useLocale;
