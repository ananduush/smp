import React from "react";
import Layout from "./index";

import messages from "../data/messages/en";

export default (props: any) => <Layout {...props} i18nMessages={messages} />;
