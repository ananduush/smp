import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";
import { getCurrentLangKey, getLangs, getUrlForLang } from "ptz-i18n";
import Header from "../components/Header";
import { graphql, useStaticQuery } from "gatsby";
import { IntlProvider } from "react-intl";
import "intl";

const Layout = ({ children, location, i18nMessages }: any) => {
  const data = useStaticQuery(
    graphql`
      query SiteMetaData {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  );

  const url = location.pathname;
  const { langs, defaultLangKey } = data?.site.siteMetadata.languages;
  const langKey = getCurrentLangKey(langs, defaultLangKey, url);
  const homeLink = `/${langKey}/`.replace(`/${defaultLangKey}/`, "/");
  const langsMenu = getLangs(langs, langKey, getUrlForLang(homeLink, url)).map(
    (item: any) => ({
      ...item,
      link: item.link.replace(`/${defaultLangKey}/`, "/"),
    })
  );

  // console.log("langKey: ", langKey);

  return (
    <IntlProvider
      locale={langKey}
      messages={i18nMessages}
      defaultLocale={defaultLangKey}
    >
      <div>
        <Helmet
          title="Gatsby Default Starter"
          meta={[
            { name: "description", content: "Sample" },
            { name: "keywords", content: "sample, something" },
          ]}
        />

        <Header />
        <div>{children}</div>
      </div>
    </IntlProvider>
  );
};

export default Layout;
