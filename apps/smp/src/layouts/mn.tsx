import React from "react";
import Layout from "./index";

import messages from "../data/messages/mn";

export default (props: any) => <Layout {...props} i18nMessages={messages} />;
