import React from "react";
import Layout from "@/components/Layout";
import AboutUs from "@/components/aboutUs";
import "@/styles/global.css";

export default function Home() {
  return (
    <main>
      <Layout title="About Us">
        <AboutUs />
      </Layout>
    </main>
  );
}
