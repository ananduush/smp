import Layout from "@/components/Layout";
import React from "react";
import Apps from "@/components/Applications";

function Technology() {
  return (
    <div>
      <Layout title="Applications">
        <Apps />
      </Layout>
    </div>
  );
}

export default Technology;
