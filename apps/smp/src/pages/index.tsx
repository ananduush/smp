import React from "react";
import Layout from "@/components/Layout";
import "../styles/global.css";
import Main from "@/components/Home";

export default function Home({}) {
  return (
    <Layout title="Home">
      <main>
        <Main />
      </main>
    </Layout>
  );
}
