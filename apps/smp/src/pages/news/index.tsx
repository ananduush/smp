import React from "react";
import News from "../../components/Carts/NewsCart";
import Layout from "../../components/Layout";
import usePosts from "../../hooks/usePosts";
import useLocale from "../../hooks/useLocale";
export default function Home({}) {
  const posts = usePosts();
  const locale = useLocale();

  return (
    <Layout title="News">
      <main className="xl:container">
        <div className="px-[5%] relative lg:px-0 mt-[2rem]">
          <section className="columns-1 sm:columns-2 lg:columns-3 min-h-full break-after-avoid break-before-avoid	break-inside-avoid	">
            {posts.map(
              (post: any) =>
                post.localization.code === locale && (
                  <News key={post.id} data={post} />
                )
            )}
          </section>
        </div>
      </main>
    </Layout>
  );
}
