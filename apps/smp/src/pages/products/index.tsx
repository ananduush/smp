import React from "react";
import Layout from "../../components/Layout";
import Content from "@/components/Products";

export default function Products() {
  return (
    <main>
      <Layout title="Products">
        <Content />
      </Layout>
    </main>
  );
}
