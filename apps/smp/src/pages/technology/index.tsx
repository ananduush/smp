import Layout from "@/components/Layout";
import Tech from "@/components/Technology";
import React from "react";

function Technology() {
  return (
    <div>
      <Layout title="Technology" activeTab="mommy">
        <Tech />
      </Layout>
    </div>
  );
}

export default Technology;
