export type DateString = string | Date;

export type S3Image = {
  extension: string;
  filesize: number;
  height: number;
  id: string;
  width: number;
};

export type PostType = {
  id: number | string;
  title: string;
  content?: any;
  contentFull?: any;
  publishDate: DateString;
  featureImage?: S3Image;
  localization: any;
};
