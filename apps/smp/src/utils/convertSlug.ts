const alpa: any = {
  а: "a",
  б: "b",
  в: "v",
  г: "g",
  д: "d",
  е: "e",
  ё: "yo",
  ж: "j",
  з: "z",
  и: "i",
  й: "i",
  к: "k",
  л: "l",
  м: "m",
  н: "n",
  о: "o",
  ө: "ö",
  п: "p",
  р: "r",
  с: "s",
  т: "t",
  у: "u",
  ү: "ü",
  ф: "f",
  х: "h",
  ц: "ts",
  ч: "ch",
  ш: "sh",
  щ: "shs",
  ъ: "ht",
  ы: "rii",
  ь: "ʹ",
  э: "e",
  ю: "yu",
  я: "ya",
};

const convertSlug = (title: string) => {
  // const tmp = title?.toLowerCase()?.trim()?.replace(/\/$/, "");
  const tmp = title?.toLowerCase()?.trim()?.replace(/ +/g, "-");
  const strings = [...tmp]?.map((c) => {
    const converted = alpa[c?.toLocaleLowerCase()];
    return converted || c;
  });
  return strings?.join("");
};

export default convertSlug;
