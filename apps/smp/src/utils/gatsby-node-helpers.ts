import languages from "../data/languages";
// Remove trailing slashes unless it's only "/", then leave it as it is
export const replaceTrailing = (path: string) =>
  path === `/` ? path : path.replace(/\/$/, ``);

// Remove slashes at the beginning and end
export const replaceBoth = (_path: string) => _path.replace(/^\/|\/$/g, "");

// If the "lang" is the default language, don't create a prefix. Otherwise add a "/$path" before the slug (defined in "locales")
export const localizedSlug = (locale: string, slug: string) =>
  languages.defaultLangKey === locale
    ? `/${locale}/${slug}`
    : `/${locale}/${slug}`;
