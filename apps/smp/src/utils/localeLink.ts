import languages from "../data/languages";

// pass location from page as props
const localeLink = (location: any, to: any) => {
  const keys = languages.langs;

  const { pathname } = location;

  const lang = keys.find(
    (k) => pathname.includes(`/${k}/`) || pathname.includes(`/${k}`)
  );

  if (!lang) {
    return to;
  }

  return `/${lang}${to}`;
};

export default localeLink;
