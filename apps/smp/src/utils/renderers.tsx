import React from "react";
import { DocumentRendererProps } from "@keystone-6/document-renderer";

export const renderers: DocumentRendererProps["renderers"] = {
  inline: {
    bold: ({ children }) => {
      return <strong>{children}</strong>;
    },
    italic: ({ children }) => {
      return <em>{children}</em>;
    },
    link: ({ children, href }) => {
      return (
        <a
          className="text-black"
          href={href}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </a>
      );
    },
  },
  block: {
    paragraph: ({ children }) => {
      return (
        <p className="" style={{ paddingBottom: "1rem" }}>
          {children}
        </p>
      );
    },
    blockquote: ({ children }) => {
      return (
        <blockquote className="mb-6 border-l-4 border-theme text-black">
          <div className="text-[28px] leading-[42px]  pl-4">{children}</div>
        </blockquote>
      );
    },
    list: ({ children, type }) => {
      let i = 0;
      if (type === "ordered") {
        return (
          <ol>
            {" "}
            {children.map((child) => (
              <li className="mb-4" key={children.indexOf(child)}>
                {child}
              </li>
            ))}
          </ol>
        );
      } else {
        return (
          <ul className="list-inside list-disc ">
            {children.map((child) => (
              <li className="mb-4" key={children.indexOf(child)}>
                {child}
              </li>
            ))}
          </ul>
        );
      }
    },
    heading: ({ children, level }) => {
      return (
        <h3 className="mb-6 lg:text-[48px] text-[32px] leading-[48px]  text-dark font-[500]">
          {children}
        </h3>
      );
    },
  },
};
