/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,jsx,ts,tsx}",
    "./src/components/**/*.{js,jsx,ts,tsx}",
    "./src/builder/**/*.{js,jsx,ts,tsx}",
  ],

  theme: {
    extend: {
      transitionTimingFunction: {
        "in-expo": "cubic-bezier(.19,1,.22,1)",
      },
      dropShadow: {
        cart: "0px 8px 24px 0px rgb(0, 0, 0 / 8%)",
      },
      boxShadow: {
        "3xl": "0px 8px 24px 0px rgb(0 0 0 / 8%)",
      },
      colors: {
        theme: "#AA3511",
        lightTheme: "#CE6F52",
        lightGray: `#F9FBFC`,
        gray: `#B3B3B3`,
        black: `#000000`,
      },

      fontFamily: {
        main: ["Inter", "sans-serif"],
      },

      lineHeight: {
        mobile: "30px",
        12: "3rem",
      },
      transitionProperty: {
        width: "width",
      },
    },
    container: {
      center: true,

      screens: {
        "2xl": "1300px",
      },
    },

    listStyleType: {
      none: "none",
      disc: "disc",
      decimal: "decimal",
      square: "square",
      roman: "upper-roman",
    },

    fontSize: {
      xs: ".75rem",
      sm: ".875rem",
      tiny: ".875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
      "7xl": "5rem",
    },

    transitionProperty: {
      width: "width",
    },
  },
  plugins: [],
};
