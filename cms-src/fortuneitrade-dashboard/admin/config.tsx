import React from "react";

import { AdminConfig } from "@keystone-6/core/types";
import { CustomNavigation } from "./components/CustomNavigation";

function CustomLogo() {
  return <h3>Steppe CMS</h3>;
}

export const components: AdminConfig["components"] = {
  Logo: CustomLogo,
  Navigation: CustomNavigation,
};
