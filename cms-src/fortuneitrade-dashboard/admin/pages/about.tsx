import { PageContainer } from "@keystone-6/core/admin-ui/components";
export default function AboutPage() {
  return (
    <PageContainer header="Системийн тухай">
      <p>
        <code>hello@steppelink.mn</code>
      </p>
    </PageContainer>
  );
}
