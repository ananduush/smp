import React from "react";
import {
  NotEditable,
  component,
  fields,
} from "@keystone-6/fields-document/component-blocks";

// naming the export componentBlocks is important because the Admin UI
// expects to find the components like on the componentBlocks export
export const componentBlocks = {
  image: component({
    preview: (props) => {
      return (
        <div>
          <NotEditable>
            {props.fields?.imageSrc?.value?.data?.image?.publicUrl && (
              <img
                src={props.fields?.imageSrc?.value?.data?.image?.publicUrl}
                alt="not found :P"
                style={{
                  width: "100%",
                }}
              />
            )}
          </NotEditable>
        </div>
      );
    },
    label: "Image",
    schema: {
      imageSrc: fields.relationship({
        listKey: "Image",
        label: "Image",
        selection: "image { publicUrl }",
      }),
    },
  }),
};
