/*
Welcome to Keystone! This file is what keystone uses to start the app.

It looks at the default export, and expects a Keystone config object.

You can find all the config options in our docs here: https://keystonejs.com/docs/apis/config
*/

import { config } from "@keystone-6/core";

import dotenv from "dotenv";
dotenv.config();

// Look in the schema file for how we define our lists, and how users interact with them through graphql or the Admin UI
import { lists } from "./schema";

// Keystone auth is configured separately - check out the basic auth setup we are importing from our auth file.
import { withAuth, session } from "./auth";
import { StorageConfig } from "@keystone-6/core/types";

const databaseUrl = process.env.DATABASE_URL;

if (!databaseUrl) {
  throw new Error("The DATABASE_URL environment variable must be set");
}

const bucketName = process.env.S3_BUCKET_NAME || "cdn.sites.steppelink.mn";
const region = process.env.S3_REGION || "ap-southeast-1";

const accessKeyId = process.env.S3_ACCESS_KEY_ID;
const secretAccessKey = process.env.S3_SECRET_ACCESS_KEY;

const siteName = process.env.SITE_NAME || "unknown-site";

const storageConfig: Record<string, StorageConfig> = {
  my_local_images: {
    // Images that use this store will be stored on the local machine
    kind: "local",
    // This store is used for the image field type
    type: "image",
    // The URL that is returned in the Keystone GraphQL API
    generateUrl: (path: string) => `http://localhost:3000/images${path}`,
    // The route that will be created in Keystone's backend to serve the images
    serverRoute: {
      path: "/images",
    },
    storagePath: "public/images",
  },
};

if (accessKeyId && secretAccessKey) {
  storageConfig.s3 = {
    kind: "s3",
    // This store is used for the file field type
    type: "image",
    // The S3 bucket name pulled from the S3_BUCKET_NAME environment variable
    bucketName,
    // The S3 bucket region pulled from the S3_REGION environment variable
    region,
    // The S3 Access Key ID pulled from the S3_ACCESS_KEY_ID environment variable
    accessKeyId,
    // The S3 Secret pulled from the S3_SECRET_ACCESS_KEY environment variable
    secretAccessKey,
    // The S3 links will be signed so they remain private
    signed: { expiry: 5000 },
    pathPrefix: `${siteName}/`,
  };
}

export default withAuth(
  // Using the config function helps typescript guide you to the available options.
  config({
    // the db sets the database provider - we're using sqlite for the fastest startup experience
    db: {
      provider: "postgresql",
      url: databaseUrl,
    },
    // This config allows us to set up features of the Admin UI https://keystonejs.com/docs/apis/config#ui
    ui: {
      // For our starter, we check that someone has session data before letting them see the Admin UI.
      isAccessAllowed: (context) => !!context.session?.data,
    },
    lists,
    session,
    storage: storageConfig,
  })
);
