import { ImageOptions } from "./types";
export declare const convertToBase64: (source: string) => string;
export declare const hexToRgbA: (hex: string, _alpha?: number) => {
    r: number;
    g: number;
    b: number;
    alpha: number;
};
export declare const parseEdits: (options?: ImageOptions) => any;
//# sourceMappingURL=helper.d.ts.map