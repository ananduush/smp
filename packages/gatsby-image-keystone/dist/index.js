"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeystoneImage = void 0;
var react_1 = __importDefault(require("react"));
var gatsby_plugin_image_1 = require("gatsby-plugin-image");
var helper_1 = require("./helper");
var BASE_URL = "https://static.steppelink.mn";
var BUCKET_NAME = "cdn.sites.steppelink.mn";
var defaultUrlBuilder = function (key, _a) {
    var options = _a.options;
    var edits = (0, helper_1.parseEdits)(options);
    var request = {
        bucket: BUCKET_NAME,
        edits: edits,
        key: key,
    };
    return "".concat(BASE_URL, "/").concat((0, helper_1.convertToBase64)(JSON.stringify(request)));
};
var defaultPlaceholder = function (key) {
    return defaultUrlBuilder(key, {
        baseUrl: BASE_URL,
        width: 4,
        height: 4,
        format: "auto",
        options: {
            resize: "cover",
            width: 4,
            height: 4,
            blur: 10,
        },
    });
};
var getKeystoneImageData = function (_a) {
    var image = _a.image, width = _a.width, height = _a.height, layout = _a.layout, backgroundColor = _a.backgroundColor, breakpoints = _a.breakpoints, formats = _a.formats, aspectRatio = _a.aspectRatio, options = _a.options, urlBuilder = _a.urlBuilder, baseUrl = _a.baseUrl, placeholderURL = _a.placeholderURL;
    return (0, gatsby_plugin_image_1.getImageData)({
        baseUrl: baseUrl,
        sourceWidth: image.width,
        sourceHeight: image.height,
        width: width,
        height: height,
        layout: layout,
        backgroundColor: backgroundColor,
        breakpoints: breakpoints,
        formats: formats,
        aspectRatio: aspectRatio,
        options: options,
        urlBuilder: urlBuilder,
        pluginName: "gatsby-image-keystone",
        placeholderURL: placeholderURL,
    });
};
var KeystoneImage = function (_a) {
    var image = _a.image, width = _a.width, height = _a.height, layout = _a.layout, backgroundColor = _a.backgroundColor, aspectRatio = _a.aspectRatio, options = _a.options, formats = _a.formats, breakpoints = _a.breakpoints, urlBuilder = _a.urlBuilder, baseUrl = _a.baseUrl, placeholderURL = _a.placeholderURL, props = __rest(_a, ["image", "width", "height", "layout", "backgroundColor", "aspectRatio", "options", "formats", "breakpoints", "urlBuilder", "baseUrl", "placeholderURL"]);
    var fileKey = "".concat(baseUrl || "unknown-site", "/").concat(image.key);
    var imageData = getKeystoneImageData({
        image: image,
        width: width,
        height: height,
        layout: layout,
        backgroundColor: backgroundColor,
        formats: formats,
        breakpoints: breakpoints,
        aspectRatio: aspectRatio,
        options: options,
        urlBuilder: urlBuilder
            ? urlBuilder
            : function (params) { return defaultUrlBuilder(fileKey, params); },
        baseUrl: baseUrl,
        placeholderURL: placeholderURL
            ? placeholderURL
            : defaultPlaceholder(fileKey),
    });
    var alt = props.alt || "";
    return react_1.default.createElement(gatsby_plugin_image_1.GatsbyImage, __assign({ image: imageData, alt: alt }, props));
};
exports.KeystoneImage = KeystoneImage;
// module.exports = {
//   KeystoneImage,
// };
