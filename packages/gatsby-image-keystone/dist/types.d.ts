import { GatsbyImageProps, IGetImageDataArgs, IUrlBuilderArgs } from "gatsby-plugin-image";
export interface KeystoneImageObj {
    key: string;
    alt: string | null;
    height: number;
    width: number;
}
export interface ImageOptions {
    width?: number;
    height?: number;
    resize?: "cover" | "contain" | "fill" | "inside" | "outside";
    fillColor?: string;
    backgroundColor?: string;
    grayscale?: boolean;
    flip?: boolean;
    flop?: boolean;
    negative?: boolean;
    flatten?: boolean;
    normalize?: boolean;
    tint?: {
        r: number;
        g: number;
        b: number;
    };
    smartCrop?: {
        faceIndex?: number;
        padding?: number;
    };
    quality?: number;
    blur?: number;
}
export interface KeystoneImageProps<OptionsType = ImageOptions> extends Omit<IGetImageDataArgs<OptionsType>, "baseUrl" | "urlBuilder">, Omit<GatsbyImageProps, "image"> {
    image: KeystoneImageObj;
    urlBuilder?: (args: IUrlBuilderArgs<OptionsType>) => string;
    baseUrl?: string;
}
//# sourceMappingURL=types.d.ts.map